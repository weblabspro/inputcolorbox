/**
 * colorpicker-input-color-box file.
 */
(function($) {
	$.prototype.extend({
		inputColorBox : function() {
			return $(this).ColorPicker({
				onSubmit: function(hsb, hex, rgb, el) {
					var color="#"+hex;
					$(el).ColorPickerHide();
					$(el).val(color).change();
					$(el).css("background", color);
				},
				onBeforeShow: function () {
					$(this).ColorPickerSetColor(this.value);
				}
			})
			.bind("keyup", function(){
				$(this).ColorPickerSetColor(this.value);
			});
		}
	});
})(jQuery);


